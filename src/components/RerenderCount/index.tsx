import React from "react";
import useForceUpdate from "../../utils/useForceUpdate";
import useRenderCounter from "../../utils/useRenderCounter";

type Props = {
    disableUpdateBtn?: boolean;
}

const RerenderCount: React.FC<Props> = ({disableUpdateBtn}) => {
    const renderCounter = useRenderCounter()
    const update = useForceUpdate()

    return (
        <div className="rerender-component-wrapper">
            <div className="flex flex-center subtitle">
                How much times this component rerendered?
                <div className="rerender-number">
                    {renderCounter}
                </div>
            </div>
            {disableUpdateBtn ? null : <button className="btn small" onClick={update}>Update</button>}
        </div>
    )
}

export default RerenderCount
