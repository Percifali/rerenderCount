import React, {useEffect} from "react";
import RerenderCount from "../RerenderCount";

const IntervalExample: React.FC = () => {
    const [count, setCount] = React.useState(0);

    useEffect(() => {
        const interval = setInterval(() => {
            setCount((prev) => prev + 1)
        }, 1000)

        return () => {
            clearInterval(interval);
        }
    }, [])

    return (
        <>
            <div className="subtitle">This interval example will change count state every one second</div>
            <div className="subtitle">Forever...</div>
            <div className="flex flex-center subtitle">
                Count <div className="rerender-number">{count}</div>
            </div>
            <RerenderCount disableUpdateBtn/>
        </>
    )
}

export default IntervalExample;
