import React from "react";
import useForceUpdate from "../../utils/useForceUpdate";
import RerenderCount from "../../components/RerenderCount";

const ParentExample: React.FC = () => {
    const update = useForceUpdate()

    return (
        <>
            <div className="subtitle">This is counter in parent example</div>
            <button className="btn small" onClick={update}>update parent</button>
            <RerenderCount disableUpdateBtn/>
        </>
    )
}

export default ParentExample;
