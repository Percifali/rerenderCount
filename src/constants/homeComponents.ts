import React from "react";
import RerenderCount from "../components/RerenderCount";
import ParentExample from "../components/ParentExample";
import IntervalExample from "../components/IntervalExample";

export type HomeComponentsNames = "simple" | "parent" | "interval";

export const HomeComponents: Record<HomeComponentsNames, React.FC> = {
    simple: RerenderCount,
    parent: ParentExample,
    interval: IntervalExample,
}
