import React from 'react';
import './App.css';
import {HomeComponents, HomeComponentsNames} from "./constants/homeComponents";

function App() {
    const [activeComponent, setActiveComponent] = React.useState<HomeComponentsNames>("simple");

    const handlePageChange = (page: HomeComponentsNames) => setActiveComponent(page);

    const Handler = HomeComponents[activeComponent];

    return (
        <div className="app">
            <div className="flex flex-center title">Hello guys from <div className="compire-logo">&nbsp;Compire</div></div>
            <div className="subtitle">-- Choose your poison --</div>
            <div className="flex flex-center btn-wrapper">
                {Object.keys(HomeComponents)
                    .map((component) =>
                    <button key={component}
                            className={`btn ${component === activeComponent ? "active" : ''}`}
                            onClick={() => handlePageChange(component as HomeComponentsNames)}>
                        {component}
                    </button>)
                }
            </div>
            <div className="component-wrapper">
                <Handler/>
            </div>
        </div>
    )
}

export default App;
