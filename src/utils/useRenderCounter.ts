import {useEffect, useRef} from "react";
import isDev from "./isDev";

const useRenderCounter = () => {
    const counter = useRef<number | null>(isDev ? null : 0);

    useEffect(() => {
        counter.current = counter.current === null ? 0 : counter.current + 1
    })

    return `${counter.current}`;
};

export default useRenderCounter;
