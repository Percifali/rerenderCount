import React from "react";

function useForceUpdate(){
    const [, updateState] = React.useState<any>();
    return () => updateState({});
}

export default useForceUpdate;
